#include "BullCowCartridge.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
	Super::BeginPlay();
	
	ValidWords = GetValidWords(Words);
	SetUpGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	// If the game is over, check whether they want to play again
	if (Input.ToLower() != "y" && bGameOver)
	{
		FGenericPlatformMisc::RequestExit(false); // Quits the game (and the editor, so be careful!)
	}
	else if (bGameOver)
	{
		SetUpGame();
	}
	else 
	{
		ProcessGuess(Input);
	}
}

void UBullCowCartridge::SetUpGame()
{
	ClearScreen();
	bGameOver = false;

	// Set HiddenWord to random word from wordlist TArray
	HiddenWord = ValidWords[FMath::RandRange(0, ValidWords.Num()-1)]; // Set the HiddenWord
	WordLength = HiddenWord.Len();
	Lives = WordLength * 2;

	// PrintLine(TEXT("The hidden word is %s."), *HiddenWord); // Debug

	PrintLine(TEXT("Welcome to Bulls and Cows!"));
	
	PrintLine(TEXT("We want you to guess a %i letter isogram."), WordLength);
	PrintLine(TEXT("An isogram is a word with no repeating letters."));
	PrintLine(TEXT("You have %i lives."), Lives);
	PrintLine(TEXT("Bulls are the right letter in the right place."));
	PrintLine(TEXT("Cows are the right letter in the wrong place."));

	PrintLine(TEXT("Press tab to activate and deactivate the keyboard.\nType in your guess and press enter to continue..."));
}

/**
 * Function to set bGameOver to true, and ask the player if they want to play again
 */
void UBullCowCartridge::EndGame(const bool& bWin)
{
	ClearScreen();
	
	bGameOver = true;

	// Check if the player has won to determine the game over message
	if (bWin)
	{
		PrintLine(TEXT("Well Done!"));
		PrintLine(TEXT("You guessed the correct Isogram!"));
	}
	else
	{
		PrintLine(TEXT("GAME OVER"));
		PrintLine(TEXT("You have run out of lives."));
		PrintLine(TEXT("\nThe hidden word was %s."), *HiddenWord);
	}

	// Ask player if they would like to play again
	PrintLine(TEXT("\nWould you like to play again?"));
	PrintLine("Enter Y or N and press enter");
}

/**
 * Function to process the players guess
 * It will end the game with either a win state or a lose state
 */
void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
	if (Guess == HiddenWord)
	{
		EndGame(true);
		return;
	}
	
	if (Guess.Len() != WordLength)
	{
		PrintLine(TEXT("Your guess is not %i characters long!"), WordLength);
		return;
	}

	// Check if Input is an isogram
	if (!IsIsogram(Guess))
	{
		PrintLine(TEXT("Your guess is not an isogram!"));
		return;
	}

	--Lives;

	// Check amount of lives if past decrement
    if (Lives > 0)
	{
    	PrintLine(TEXT("Oh no..."));
		PrintLine(TEXT("You got it wrong."));
		PrintLine(TEXT("You have %i Lives remaining."), Lives);
    	const FBullCowCount Score = GetBullCows(Guess);

		PrintLine(TEXT("You have %i Bulls and %i Cows"), Score.Bulls, Score.Cows);
	}
	else
	{
		EndGame(false);
	}

}
/*
 * Function to detect whether Word is an isogram
 */
bool UBullCowCartridge::IsIsogram(const FString& Word)
{
	// Cycle through the string and find any repeating characters
	for (int32 Index = 0; Index < Word.Len(); Index++)
	{
		for (int32 Comparison = Index + 1; Comparison < Word.Len(); Comparison++)
	 	{
	 		if (Word[Index] == Word[Comparison])
	 		{
	 			return false;
	 		}
	 	}
	}
	return true;
}

/*
 * Get all the valid words in the word-list and return an array with the valid words in
 */
TArray<FString> UBullCowCartridge::GetValidWords(const TArray<FString>& CheckWords)
{
	TArray<FString> Valid;

	for (auto Word : CheckWords)
	{
		if (Word.Len() >= 4 && Word.Len() <= 6 && IsIsogram(Word))
		{
			Valid.Emplace(Word);
		}
	}
	return Valid;
}

FBullCowCount UBullCowCartridge::GetBullCows(const FString& Guess) const
{
	//Remove previous Bulls and Cows
	FBullCowCount Count;

	for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); ++GuessIndex)
	{
		if (Guess[GuessIndex] == HiddenWord[GuessIndex])
		{
			++Count.Bulls;
			continue;
		}

		for (int32 CowIndex = 0; CowIndex < HiddenWord.Len(); ++CowIndex)
		{
			if (Guess[GuessIndex] == HiddenWord[CowIndex])
			{
				++Count.Cows;
				break;
			}
		}
	}
	return Count;
}
